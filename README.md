# openDesk License Compliance Artefacts

## Status

SBOMs are provided in folder [sboms/0.5.37](./sboms/0.5.37/) or [sboms/0.5.74](./sboms/0.5.74/).

The SBOMs are organized by supplier name as specified in image-index files:
* [image-index-0.5.37.json](./sboms/0.5.37/image-index-0.5.37.json)
* [image-index-0.5.74.json](./sboms/0.5.74/image-index-0.5.74.json)

These are taken from the release artefacts of the corresponding openDesk version:
* [Release 0.5.37](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/releases/v0.5.37).
* [Release 0.5.74](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/releases/v0.5.74).

The primary objective is to convey all identified licenses from the license scan. The identified licenses have been
analysed to comply to the policies of Open CoDE. A respective deviation report was created to provide an overview of
the identified issues:
* [Deviation Report 0.5.37](./sboms/openDesk%20Deviation%20Report-0.5.37.md)
* [Deviation Report 0.5.74](./sboms/openDesk%20Deviation%20Report-0.5.74.md)

## Disclaimer

* Version 0.5.37 and 0.5.74 of openDesk and its corresponding SBOMs are only preliminary. The list of containers and
  their content is not final.

* Current outputs are automated based on a container processing pipeline specialised on Open CoDE
  policies. Only selected artefacts have been curated to avoid false identifications.

* The SBOMs are provided AS-IS without expressing or implying any warranties or conditions of any kind.

* The SBOMs are not guaranteed to be correct or complete. The primary objective is to convey all identified licenses
  from an automated license scan.

* Concluded licenses are primarily derived in an automated manner. The default rule is to prefer permissive licenses
that reduce potential conflicts due to license incompatibilities.

* The SBOMs do not convey any opinion or legal advice.

* The SBOMs are not sufficient to fulfill the license obligations associated with the identified licenses when using or
  distributing the software.

## Process Outline

For a better understanding two process parts of the pipeline are depicted.

### Container Scan

The following illustration outlines the process for obtaining and scanning containers.

![container-scan-process.png](media/container-scan-process-drawio.png){width=80%}

In order to influence the results of the scan either
* Patterns & Rules need to be extended (general and/or specific patterns), or
* the license model has to be adjusted.

### SBOM Creation

To produce an SPDX SBOM from the results of the Container Scan step, the following process is used:

![sbom-creation-process.png](media/sbom-creation-process-drawio.png){width=50%}

Different process stages are differentiated in order to retain traceability of the performed enhancements and
modifications. The results from the intermediate steps can be examined to verify and validate appropriate mechanisms
are applied.

## Next steps

The SBOMs for openDesk version 0.5.37 and 0.5.74 only reflect an intermediate state. For upcoming versions
the following pipeline improvements are considered.

### SBOM Structure / Creation

| Description                                                           | Priority | Status                 |
|-----------------------------------------------------------------------|----------|------------------------|
| Provision of data regarding intermediate processing step              | SHOULD   | *(on supplier demand)* |
| Filter annotations in the SPDX document; convey only relevant details | SHOULD   | *(tbd)*                |

### Container Analysis

| Description                                                 | Priority | Status   |
|-------------------------------------------------------------|----------|----------|
| further improved component detection                        | MUST     | *(tbd)*  |
| syft benchmarking for validation and continuous improvement | COULD    | *(tbd)*  |
| asset level component identification quality metrics        | COULD    | *(tbd)*  |
| layer analysis; detect modifications                        | MAY      | *(tbd)*  |

### SBOM Content / Container Analysis

| Description                                                                                                                                              | Priority | Status                                    |
|----------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-------------------------------------------|
| resolve false identifications and unknown licenses                                                                                                       | MUST     | *(as-is for 0.5.37/0.5.74; tbd for further versions)* |
| eliminate NOASSERTIONs for declared and concluded licenses as far as possible; improve license model where necessary and apply curations where necessary | SHOULD   | *(as-is for 0.5.37/0.5.74; tbd for further versions)*  |
| full containment relationships                                                                                                                           | MAY      | *(as-is for 0.5.37/0.5.74; tbd for further versions)*  |
| Replace SHA-1 with SHA-256 hashes (compare BSI SBOM TR)                                                                                                  | SHOULD   | *(implemented; tbd: validate for future versions)* |
