# Open CoDE Licence Compliance Deviation Report for openDesk 0.5.74

[TOC]

# License Deviations

## License Deviation - Elastic License 2.0

**Identification**

A reference to the Elastic License 2.0 was identified in the file

* `hazelcast-5.3.1.jar`

**Affected Assets**

| Supplier        | Asset Identification                                                                          |
|-----------------|-----------------------------------------------------------------------------------------------|
| Open-Xchange AG | documentconverter@<br>bd11b4e5a62377aab79ebc0ebbe8da0bf54d42ce9a8ae64db0c84608570edf9f        |
| Open-Xchange AG | imageconverter@<br>590a8a4c583057f6bb071247c2f8b8566c79d5d219482dcaa452b30c944c876b           |
| Open-Xchange AG | middleware-public-sector@<br>4a9cc9d6745b09a9ace2475fbbacfeff2ca66db02b6314eb8e035f28e28574a8 |

**Deviation Details**

The full license text was identified in file:
`opt/open-xchange/bundles/com.hazelcast/lib/hazelcast-5.3.1.jar/META-INF/THIRD-PARTY.txt`
The identified license is not approved by Open CoDE, as a general right to distribute the software is not granted.

**Justification**

The artefact has been examined whether it includes software parts under the given license. It was concluded that no
contained part falls under the Elastic-2.0 license. The identification is a false-positive which applies to an optional
dependency, only.

**Limitation & Restrictions**

Based on the current justification, no limitations and restrictions are to be anticipated.

**Compensating & Corrective Measures**

The supplier was requested to provide a statement whether the artefact is required. In response, the supplier identified
hazelcast as required at runtime. The artefact cannot be removed in the short-term.

In the mid- or long-term clear attribution policies shall be provided to enable the community to clearly differentiate
parts and licenses upstream at the source.

## License Deviation – Paul Hsieh Derivative

**Identification**

The license remark / notice was identified in text in various packages including Libhashkit2 and Perl (modules and
libs). See also [Paul Hsieh License Identification Details](#paul-hsieh-license-identification-details).

**Affected Asset**

The license can be identified in Ubuntu/Debian based container images distributed over container images of all suppliers
except for XWiki.

**Deviation Details**

The Paul Hsieh Derivative License is not approved by Open CoDE as stand-alone license. The Paul Hsieh Derivative
License needs to be understood in context. Based on the content provided on
http://www.azillionmonkeys.com/qed/weblicense.html the following conclusion is drawn.

1) In case another license is provided the Paul Hsieh Derivative and Paul Hsieh Exposition license do not apply.
2) For raw source code taken from the website the BSD 3-Clause License (Paul Hsieh variant) applies. Downstream use and
   distribution of binaries and source code is possible within the scope of this license.
3) Content (as opposed to source code and compiled binaries) – in case 1 does not apply – must be managed with respect
   to the Paul Hsieh Exposition License.

In case the Paul Hsieh Derivative License is identified and effective, the above cases must be verified.

**Justification**

**Findings in Perl -**
The license information details in the copyright file are outdated and do not reflect the current package status.
See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1054457. The referenced issue provides evidence that the
problematic hash algorithm has been removed already for version v5.25.8. Yet, the copyright file was not adapted
accordingly. For the Perl versions used by openDesk container images the Paul Hsieh Derivative License (despite being
documented in the copyright file of the Perl packages) does not apply and are not regarded effective. The finding is
therefore argued to be a false positive in the Perl case, that can be ignored.

**Findings in Libhashkit2 / Libmemcached -**
Libhashkit2 uses the `libhashkit/hsieh.cc`. The file has the following header:

    Copyright: Copyright (c) 2004, 2005, Paul Hsieh
    License: Paul-Hsieh
    http://www.azillionmonkeys.com/qed/weblicense.html
    The derivative content includes raw computer source code, ideas, opinions,
    and excerpts whose original source is covered under another license and
    transformations of such derivatives. Note that mere excerpts by themselves
    (with the exception of raw source code) are not considered derivative works
    under this license. Use and redistribution is limited to the following
    conditions:
    * One may not create a derivative work which, in any way, violates the
      Paul Hsieh exposition license described above on the original content.
    * One may not apply a license to a derivative work that precludes anyone
      else from using and redistributing derivative content.
    * One may not attribute any derivative content to authors not involved in
      the creation of the content, though an attribution to the author is not
      necessary.

No other license is provided with the file and no provenance is provided that the code was taken from the
azillionmonkeys web site. The information regarding the LGPL found in http://www.azillionmonkeys.com/qed/hash.html is
considered insufficient. In this case the Paul Hsieh Derivative License applies. Evidence is also given by examining
the source files of the Debian package.

Unfortunately, the file is contained in the source and no evidence was found that the file is left out by the package
build procedure.

Libhashkit2/Libmemcached are used in the following assets:

| Supplier        | Asset Identification                                                            |
|-----------------|---------------------------------------------------------------------------------|
| Univention GmbH | umc-server@<br>4de1e4164cd442cfd2e9fe0d8cc144e73872a53f493a64d735f5c635715e1437 |

**Limitation & Restrictions**

The required grants for use and distribution of the Libhashkit and Libmemcached packages in the given versions are
not provided. The affected containers are incompliant and not fit for purpose in public administration. Effectively,
the affected packages cannot be used and distributed.

**Compensating & Corrective Measures**

Univention GmbH is required to remove or replace the Libmemcached packages.

## License Deviation - BSD 3-Clause License (clear)

**Identification**

Within supplier-provided Container Images the license was identified in the package:

* `libsvtav1enc1-1.4.1+dfsg-1`

The license is covering the AV1 codec provided by the package.

**Affected Asset**

| Supplier                   | Asset Identification                                                                                        |
|----------------------------|-------------------------------------------------------------------------------------------------------------|
| Element c/o New Vector Ltd | nginx@<br>9504f3f64a3f16f0eaf9adca3542ff8b2a6880e6abfb13e478cca23f6380080a                                  |
| Element c/o New Vector Ltd | opendesk-element-web@<br>0595292e824c039e9c088a845b3d49c6be93d46f9f99090783eb20cb1fc27227                   |
| Univention GmbH            | guardian-management-ui-management-ui@<br>e1e4e1e7fa0c7ffff09e63474b5b054cb492fbb743cad0b2ee5910bb1de6967b   |
| openDesk                   | openDesk opendesk-nextcloud-management@<br>969bdaaa24ef6091ecb8b63b4fb2f7925fa10eaf46d3997ec74f6414ac373a8d |
| openDesk                   | openDesk opendesk-nextcloud-php@<br>21e8584f10f19b263be76a93df2658e2e845e00548d1b176ee336eb1f0e15a50        |

**Deviation Details**

In the case of libsvtav1enc1-1.4.1+dfsg-1 it can be observed that a combination of licenses is used. The BSD 3-Clause
License applies for the source code and binary but does not grant any rights to use associated patents, which is the
reason the license is Open CoDE unapproved. In addition, the Alliance for Open Media Patent License 1.0 applies.
The Alliance for Open Media Patent License 1.0 explicitly grants sufficient rights for patents associated with the
package:

    [...]
    1.1. Patent License. Subject to the terms and conditions of this License, each
         Licensor, on behalf of itself and successors in interest and assigns,
         grants Licensee a non-sublicensable, perpetual, worldwide, non-exclusive,
         no-charge, royalty-free, irrevocable (except as expressly stated in this
         License) patent license to its Necessary Claims to make, use, sell, offer
         for sale, import or distribute any Implementation.
    [...]

**Justification**

The combination of the two licenses compensates the missing patent right grants of the BSD 3-Clause License (clear).

**Limitation & Restrictions**

Based on the licensing terms and conditions of the two associated licenses individual obligations must be addressed.
Specifically, with the Alliance for Open Media Patent License 1.0 licensor and licensee relationship is different than
observed with open-source licenses.

In the context of openDesk the obligations are concluded to not impose inappropriate limitations and/or restrictions.

**Compensating & Corrective Measures**

The libsvtav1enc1 package has been identified as required to provide certain openDesk functionality (rendering of videos
supplied by users). No measures are taken by the affected suppliers to remove the package.

Alliance for Open Media Patent License 1.0 is not approved by Open CoDE. Currently, it is assumed that the approval may
require an additional license to be present, since the patent license does not cover code in source or binary form.

Open CoDE should differentiate between different licensing subjects. See also 4.2. An adoption is possible mid- to
long-term.

## License Deviation - Tiger Cryptography License

**Identification**

A reference to the license was identified in bouncy castle artefacts. Currently identified variants are:

* `bcprov-ext-jdk15on-1.69.jar`
* `bcprov-jdk18on-1.74.jar`
* `bcprov-jdk18on-1.75.jar`
* `bcprov-jdk18on-1.77.jar`
* `org.bouncycastle.bcprov-jdk15on-1.74.jar`

**Affected Asset**

| Supplier                     | Asset Identification                                                                          |
|------------------------------|-----------------------------------------------------------------------------------------------|
| Open-Xchange AG              | documentconverter@<br>bd11b4e5a62377aab79ebc0ebbe8da0bf54d42ce9a8ae64db0c84608570edf9f        |
| Open-Xchange AG              | imageconverter@<br>590a8a4c583057f6bb071247c2f8b8566c79d5d219482dcaa452b30c944c876b           |
| Open-Xchange AG              | middleware-public-sector@<br>4a9cc9d6745b09a9ace2475fbbacfeff2ca66db02b6314eb8e035f28e28574a8 |
| Nordeck IT + Consulting GmbH | jvb@<br>75dd613807e19cbbd440d071b60609fa9e4ee50a1396b14deb0ed779d882a554                      |
| Univention GmbH              | keycloak-keycloak@<br>1e8e45a2e01050c1473595c3b143446363016ea292b0c599ccd9f1bd37112206        |
| XWiki SAS                    | xwiki@<br>276e871e3938bf80a86a0e1e63751c843920ccd260848badafec8689410ded80                    |

**Deviation Details**

The Bouncy Castle Crypto Library uses the Tiger Digest, which contains a reference to the page
https://www.cs.technion.ac.il/~biham/Reports/Tiger/ [^1]:

    /**
     * implementation of Tiger based on:
     * <a href="https://www.cs.technion.ac.il/~biham/Reports/Tiger">
     * https://www.cs.technion.ac.il/~biham/Reports/Tiger</a>
     */

The Bouncy Castle Crypto Library is distributed under MIT License. From the above web page, the following statement was
extracted:

    Tiger has no usage restrictions nor patents. It can be used freely, with the reference implementation, with other
    implementations or with a modification to the reference implementation (as long as it still implements Tiger). We only
    ask you to let us know about your implementation and to cite the origin of Tiger and of the reference implementation.

The above statement is referred to as Tiger Cryptography License [^2].

[^1]: In case the page is not available, an archived version can be retrieved from the web archive.
See http://web.archive.org/web/20190818130147/https://www.cs.technion.ac.il/~biham/Reports/Tiger/.

[^2]: See also https://scancode-licensedb.aboutcode.org/tiger-crypto.html and
https://github.com/org-metaeffekt/metaeffekt-universe/blob/main/src/main/resources/ae-universe/%5Bt%5D/README.md.

**Justification**

Based on above observation no limitation or restrictions to the use of the Tiger Digest applies. Open CoDE has not
approved this license since it is not sufficient as stand-alone license. Bouncy Castle has consumed the TigerDigest
and created its own implementation. As such, Bouncy Castle is in alignment with the Tiger Cryptography License. Within
openDesk the source and binary are regarded to be consumed, used, and distributed under MIT License. No copyright
infringement to the original authors is observed.

**Limitation & Restrictions**

Following the justification above, no limitations and restrictions apply.

**Compensating & Corrective Measures**

Currently, non-required. In case the above justification is challenged a possibility is to remove the TigerDigest from
the Bouncy Castle Crypto Library upstream or applying a patch.


## License Deviation - ISO Permission (ISO 8879)

**Identification**

The ISO Permission (specifically the ISO 8879 Permission) was identified in the following artefacts:

* `lucene-analyzers-common-8.11.2.jar`
* `python3-pygments-2.3.1+dfsg-1+deb10u2`
* `xwiki-commons-xml-15.10.4.jar`
* `xwiki-rendering-wikimodel-15.10.4.jar`

The usage of the permission within these artefacts was examined. The usage is always in context to html or xml entity
definitions. The permission is not used for licensing code. See also [ISO Permission (ISO 8879) Identification Details](#iso-details-iso-permission--iso-8879--identification-details).

**Affected Asset**

| Supplier        | Asset Identification                                                            |
|-----------------|---------------------------------------------------------------------------------|
| Univention GmbH | umc-server@<br>4de1e4164cd442cfd2e9fe0d8cc144e73872a53f493a64d735f5c635715e1437 |
| XWiki SAS       | xwiki@<br>276e871e3938bf80a86a0e1e63751c843920ccd260848badafec8689410ded80      |

**Deviation Details**

Open CoDE has not approved the ISO Permission for code. No statement is available for definitions and specifications.
It is assumed that an approval for such content is possible.

**Justification**

Based on the provided information the ISO Permission is regarded acceptable for content such as definitions and
specifications as found in the openDesk assets.

**Limitation & Restrictions**

From the terms and conditions of the ISO Permissions no concrete limitation or restriction is inferred when applied to
definitions as identified within the openDesk assets.

**Compensating & Corrective Measures**

The affected suppliers shall further evaluate whether the files covered by the ISO Permission can be removed.
Additionally, a statement from the right holder, in this case the International Organization for Standardization (ISO),
shall be requested.

## License Deviation - UnRar License

**Identification**

The UnRar License was identified in the artefact:

* `junrar-7.5.5.jar`

located in the XWiki SAS Container at path `var/lib/jetty/webapps/ROOT/WEB-INF/lib/junrar-7.5.5.jar`.

**Affected Asset**

| Supplier  | Asset Identification                                                       |
|-----------|----------------------------------------------------------------------------|
| XWiki SAS | xwiki@<br>276e871e3938bf80a86a0e1e63751c843920ccd260848badafec8689410ded80 |

**Deviation Details**

The UnRar License is not an open-source license. It classifies as freeware license. Furthermore, the terms and
conditions of the license include a restriction in use:

    2. UnRAR source code may be used in any software to handle
       RAR archives without limitations free of charge, but cannot be
       used to develop RAR (WinRAR) compatible archiver and to
       re-create RAR compression algorithm, which is proprietary.
       Distribution of modified UnRAR source code in separate form
       or as a part of other software is permitted, provided that
       full text of this paragraph, starting from "UnRAR source code"
       words, is included in license, or in documentation if license
       is not available, and in source code comments of resulting package.

As such it is not approved by Open CoDE.

**Justification**

XWiki SAS uses the artefact to enable indexing of compressed content provided as RAR Archives. XWiki SAS does not intend
to “develop RAR (WinRAR) compatible archiver and to re-create RAR compression algorithm”. As such the restriction
imposed by the license has no concrete implication in the XWiki SAS and openDesk case.

**Limitation & Restrictions**

See Deviation Details. UnRar must not be used to “develop RAR (WinRAR) compatible archiver and to re-create RAR
compression algorithm”.

This restriction must be documented and managed.

**Compensating & Corrective Measures**

Currently no attempt is made to remove the artefact. The need for the RAR compressed content support is given by the
underlying third-party indexing engine TIKA and Solr.

## License Deviation - Unicode Terms (2004)

**Identification**

The Unicode Terms (2004) have been identified in Lucene artefacts as listed in
[Unicode Terms (2004) Identification Details](#unicode-terms--2004--identification-details).

**Affected Asset**

| Supplier  | Asset Identification                                                       |
|-----------|----------------------------------------------------------------------------|
| XWiki SAS | xwiki@<br>276e871e3938bf80a86a0e1e63751c843920ccd260848badafec8689410ded80 |

**Deviation Details**

The Unicode Terms (2004) are not approved by Open CoDE.

    Unicode, Inc. hereby grants the right to freely use the information
    supplied in this file in the creation of products supporting the
    Unicode Standard, and to make copies of this file in any form
    for internal or external distribution as long as this notice
    remains attached.

The precondition for the grants implied by the license is only given in case the resulting product is “supporting the
Unicode Standard”. This is regarded as not in alignment with the Open CoDE license approval criteria by Open CoDE.

**Justification**

For openDesk the conflicting aspect is not regarded an issue. By using Unicode encodings (which are a fundamental to
support different input files and formats) openDesk implicitly supports the Unicode Standard.

**Limitation & Restrictions**

See Deviation Details.

**Compensating & Corrective Measures**

Currently no attempt is made to remove the artefact. Lucene is a required component to support indexing of content.

The Open CoDE not approval could be revised. The use of the Unicode Terms (2004) appears not conflicting with the
conditions required for use in public administration.

## License Deviation - Bigelow and Holmes Luxi Fonts License

**Identification**

The license was identified in the OpenJDK package of Debian:

* `openjdk-11-jre-headless-11.0.20+8-1~deb11u1`

The license is explicitly identified in the package copyright file:

    Luxi fonts copyright (c) 2001 by Bigelow & Holmes Inc. Luxi font instruction
    code copyright (c) 2001 by URW++ GmbH. All Rights Reserved. Luxi is a regis-
    tered trademark of Bigelow & Holmes Inc.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of these Fonts and associated documentation files (the "Font Software"), to
    deal in the Font Software, including without limitation the rights to use,
    copy, merge, publish, distribute, sublicense, and/or sell copies of the Font
    Software, and to permit persons to whom the Font Software is furnished to do
    so, subject to the following conditions:
    [...]

**Affected Asset**

| Supplier                     | Asset Identification                                                           |
|------------------------------|--------------------------------------------------------------------------------|
| Open-Xchange AG              | gotenberg@<br>c97c1adb971d149222062ec46c5d749d710b38ad153c5c6ed954023e2401c9d0 |
| Nordeck IT + Consulting GmbH | jibri@<br>87aa176b44b745b13769f13b8e2d22ddd6f6ba624244d5354c8dd3664787e936     |
| Nordeck IT + Consulting GmbH | jicofo@<br>820fcd4b072b29f42c1c37389fbefda1065f1e9654694941485dc08123c8a93b    |
| Nordeck IT + Consulting GmbH | jvb@<br>75dd613807e19cbbd440d071b60609fa9e4ee50a1396b14deb0ed779d882a554       |

**Deviation Details**

The license is not Open CoDE approved, as it does not allow modifications:

    [...]
    The Font Software may not be modified,
    altered, or added to, and in particu- lar the designs of glyphs or characters in the Fonts may not be modified nor may
    additional glyphs or characters be added to the Fonts. This License becomes null and void when the Fonts or Font
    Software have been modified.
    [...]

**Justification**

The code and font definitions are regarded as part of the Debian OpenJDK package and are consumed only “AS IS”. No
modifications nor addons are applied.

**Limitation & Restrictions**

The limitations imposed by the license do not imply any limitations and restrictions on openDesk itself.

**Compensating & Corrective Measures**

In case the deviation is not acceptable, the OpenJDK will be replaced with another variant.

The Debian Package manger could be contacted to request further details and the current scope of use.

## License Deviation - WS-Addressing Specification License

**Identification**

The license was identified as embedded license in:

* `cxf-core-3.5.5.jar`
  located in path:
* ` opt/open-xchange/bundles/com.openexchange.soap.common/lib/cxf-core-3.5.5.jar`

**Affected Asset**

| Supplier        | Asset Identification                                                                          |
|-----------------|-----------------------------------------------------------------------------------------------|
| Open-Xchange AG | documentconverter@<br>bd11b4e5a62377aab79ebc0ebbe8da0bf54d42ce9a8ae64db0c84608570edf9f        |
| Open-Xchange AG | imageconverter@<br>590a8a4c583057f6bb071247c2f8b8566c79d5d219482dcaa452b30c944c876b           |
| Open-Xchange AG | middleware-public-sector@<br>4a9cc9d6745b09a9ace2475fbbacfeff2ca66db02b6314eb8e035f28e28574a8 |

**Deviation Details**

The WS-Addressing Specification License contains a restriction. The grants provided by the license are only given for
test and evaluation purpose. The license is explicitly not approved by Open CoDE.

**Justification**

The license is an inner license of Apache CXF and integrated in the affected container images as provided by the Apache
Foundation, respectively the Apache CXF project. The license has been approved by the legal team.

However, no evidence or backgrounds are available regarding this evaluation. See

* https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/2369#note_1391368
* https://lists.apache.org/thread/d81ccwkv1jp1tymr2lj3t4zn6z61wc4r
* https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/12924

**Limitation & Restrictions**

The license can only be used for test or evaluation purposes. Within openDesk this boundary conditions are not met.
Currently, the license appears to be not applicable for the openDesk use case.

**Compensating & Corrective Measures**

Currently, awaiting feedback from Apache CXF / Eclipse Foundation Legal team. See
https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/12924

## License Deviation - Univention Terms

**Identification**

The Univention Terms are included in various files. The Univention Terms are as follows:

    Binary versions of this program provided by Univention to you as
    well as other copyrighted, protected or trademarked materials like
    Logos, graphics, fonts, specific documentations and configurations,
    cryptographic keys etc. are subject to a license agreement between
    you and Univention and not subject to the GNU AGPL V3.

While Univention GmbH source code is licensed primarily under GNU Affero General Public License 3.0 the Univention Terms
dedicates binaries and other materials to a bilateral license agreement.

**Affected Asset**

| Supplier        | Asset Identification                                                                         |
|-----------------|----------------------------------------------------------------------------------------------|
| Univention GmbH | config-htpasswd@<br>ba4f6fa2736a789c6c7413cc784bfadbeda1b3269fee29a871207f6f2ba2ee08         |
| Univention GmbH | data-loader@<br>9978e5eae3846e3c32effb2e602136d8855aaec287fb280a54b311defab2fbf3             |
| Univention GmbH | keycloak-handler@<br>b27d76b1a397bc776b0b7e652e318fd707da4bbfba81908ed3a7ca866b2ee8cc        |
| Univention GmbH | keycloak-keycloak@<br>1e8e45a2e01050c1473595c3b143446363016ea292b0c599ccd9f1bd37112206       |
| Univention GmbH | keycloak-proxy@<br>368c57fc4e730d2ad8e24b29f4c876f7e6dbd835a9913f5d0909159409cd5042          |
| Univention GmbH | ldap-notifier@<br>bb7d76fb5299e9d019aa61b5397af15063a5b341fcf2b74c65db679ca5fa873f           |
| Univention GmbH | ldap-server@<br>abcaec050875a8605befe13cce78f9f8eb28aa3c1764e281a8540b2a3db4a5da             |
| Univention GmbH | notifications-api@<br>f058398d68c38039bb168af6d60d016f66fffde83a02f0b8f62124ebf2fed4d9       |
| Univention GmbH | ox-connector-standalone@<br>308489c0c0e0436bbbedbd757f78875d44468992c46c8d371c584dc778b30770 |
| Univention GmbH | portal-frontend@<br>97887159fc4a7febdf663761a65b7fac2eb7b99b6dd042c7d63ce6b254ea6fb9         |
| Univention GmbH | portal-listener@<br>1e03db8153cbff0825c4370526d5d44a6b9b92c643b0e605d1bfc762ebac3a31         |
| Univention GmbH | portal-server@<br>47c825f83b61799b287b11cf5c548e05000c21e7d071d1f2095fbba4c952d84c           |
| Univention GmbH | provisioning-dispatch@<br>43fda35f02cfa7c4c6aaa42e561858f7b0b80485370c1e393cef3a4b8d7715c0   |
| Univention GmbH | selfservice-invitation@<br>8dd90d8669e206232edff37aca73c528344ad453ad0154f36cca0561bf1999a2  |
| Univention GmbH | selfservice-listener@<br>de0fc94cab436e982219d9c883a2353d91de583d5cf75046902847df4b451e28    |
| Univention GmbH | store-dav@<br>4a2c7675c15a244a3a8c002e030db425cdbe5cd7bf8c21ced4bac6f5252382bd               |
| Univention GmbH | udm-rest-api@<br>94c8294130f6a187bb850bcaeb314a09c5aa48ab97e3f419fbeb6ddbd39a3246            |
| Univention GmbH | umc-server@<br>4de1e4164cd442cfd2e9fe0d8cc144e73872a53f493a64d735f5c635715e1437              |
| Univention GmbH | wait-for-dependency@<br>63451fe519d557e52d5f99e21231594daebb2990eb734931172ad61543c443cb     |

**Deviation Details**

Univention Terms and associated EULA are not in alignment with the Open CoDE policy. A way forward must be agreed by
the stakeholders.

**Justification**

None.

**Limitation & Restrictions**

Not evaluated.

**Compensating & Corrective Measures**

Univention provides the artefacts published on Open CoDE under AGPL-3.0 without further restrictions. See [Univention Open CoDE licensing notes](./Univention%20Open%20CoDE%20licensing%20notes.md) for details.

## License Deviation - Google Terms of Service

**Identification**

A reference to the Elastic License 2.0 was identified in the

* `usr/share/univention-web/js/dojox/rpc/SMDLibrary/google.smd

**Affected Asset**

| Supplier   | Asset Identification                                                             |
|------------|----------------------------------------------------------------------------------|
| Univention | umc-gateway@<br>e32cfe40cb0022d4084d89e9ae0367e559302c50d92223bd4c8905698141a3ef |

**Deviation Details**

The Google Terms of Service were detected in file `usr/share/univention-web/js/dojox/rpc/SMDLibrary/google.smd`.
The file contains the following header:

    // Google Ajax API SMD as defined by:
    // http://code.google.com/apis/ajaxsearch/documentation/reference.html
    // PLEASE Read the Google Terms of Service:
    // http://code.google.com/apis/ajaxsearch/terms.html

The Google Terms of Service are not approved by Open CoDE, while the license appears to only apply to the use of a
remote services.

The supplier was requested to evaluate whether Google services are used in the context of openDesk and whether the
software is required. The supplier confirmed that Google services are not used in the openDesk context. It will be
evaluated if the component dojox can be removed.

**Justification**

As mentioned, the license is assumed to not directly affect resources contained in dojox. The terms apply to the remote
service and its use. Once clarified that the services are not required to provide the openDesk functionality, the
presence of the reference can be regarded as not effective for openDesk and the respective container image.

**Limitation & Restrictions**

In case Google Services are required for openDesk the consumers of the container must be informed. The use of Google
Services requires a commercial relationship with Google with additional terms, conditions and pricing associated.

**Compensating & Corrective Measures**

See above. Removal of dojox might be possible.

# Further Compliance Considerations

## Pending License Assessments by Open CoDE

In the scope of the container images for openDesk licenses have been detected by a license scanner. The scanner uses a
database of modelled licenses beyond the scope of SPDX and ScanCode. During project work additional licenses as
found in the scanned subjects were actively added.

The current scan results for the openDesk container images covers license where no Open CoDE categorisation as either
approved or not approved License is available. These licenses have been collected and requested for assessment.

## Open CoDE Licensing Policy for different Subjects

For different subjects such as code, content, specifications, and patents different licenses may apply. Currently, Open
CoDE license approval does not differentiate.

A policy should be provided that covers and differentiates different licensing subjects with potentially different
policy rules for each subject.

Risks of this aspect have already been covered in the license deviation risk assessments.

## Open CoDE License Compliance Policy for Third-Party Container Images

Third-party openDesk upstream container images which are obtained from other external sources and not uploaded to the
Open CoDE (binary) repository, are currently not covered by the Open CoDE policy. However, these container images are
required to setup a deployment and may even be used downstream in production setups (while there, scalable alternatives
may be required).

It is essential to understand the policy construct for an Open CoDE hosted project in this respect.

## Modifier, Clauses, Restriction & Limitation Marker

Apart from license assessments and license scanning certain additional statements, conditions, clauses, and modifiers
may be present in the software components. Currently, such addons are not actively reflected.

The current used pipeline supports different markers enabling to identify certain phrases and modifiers. E.g. a marker
for “not for commercial use”.

To further reduce risks these markers should be reflected with Open CoDE and result in a list of not acceptable markers.

# Appendix

## Paul Hsieh License Identification Details

**Identification**

The Paul Hsieh License was identified in the following artefacts/packages:
* `libmemcached11-1.0.18-4.2`
* `libperl5.28-5.28.1-6+deb10u1`
* `libperl5.30-5.30.0-9ubuntu0.4`
* `libperl5.32-5.32.1-4+deb11u2`
* `libperl5.32-5.32.1-4+deb11u2`
* `libperl5.36-5.36.0-7`
* `libperl5.36-5.36.0-7`
* `perl-5.28.1-6+deb10u1`
* `perl-5.36.0-7`
* `perl-base-5.28.1-6+deb10u1`
* `perl-base-5.28.1-6+deb10u1`
* `perl-base-5.30.0-9ubuntu0.4`
* `perl-base-5.32.1-4+deb11u2`
* `perl-base-5.32.1-4+deb11u2`
* `perl-base-5.36.0-7+deb12u1`
* `perl-base-5.36.0-7`
* `perl-base-5.36.0-7`
* `perl-modules-5.28-5.28.1-6+deb10u1`
* `perl-modules-5.30-5.30.0-9ubuntu0.4`
* `perl-modules-5.32-5.32.1-4+deb11u2`
* `perl-modules-5.32-5.32.1-4+deb11u2`
* `perl-modules-5.36-5.36.0-7`
* `perl-modules-5.36-5.36.0-7`

**Memcached Copyright Files**

    Files: debian/*
    Copyright: Copyright (c) 2009, Monty Taylor <mordred@inaugust.com>
    Copyright (c) 2012-2014, Michael Fladischer <FladischerMichael@fladi.at>
    License: BSD-3-clause-TangentOrg
    Files: libhashkit/hsieh.cc
    Copyright: Copyright (c) 2004, 2005, Paul Hsieh
    License: Paul-Hsieh
    http://www.azillionmonkeys.com/qed/weblicense.html
    The derivative content includes raw computer source code, ideas, opinions,
    and excerpts whose original source is covered under another license and
    transformations of such derivatives. Note that mere excerpts by themselves
    (with the exception of raw source code) are not considered derivative works
    under this license. Use and redistribution is limited to the following
    conditions:
    * One may not create a derivative work which, in any way, violates the
      Paul Hsieh exposition license described above on the original content.
    * One may not apply a license to a derivative work that precludes anyone
      else from using and redistributing derivative content.
    * One may not attribute any derivative content to authors not involved in
      the creation of the content, though an attribution to the author is not
      necessary.

**Perl Copyright Files**

    License: HSIEH-DERIVATIVE
    The derivative content includes raw computer source code, ideas,
    opinions, and excerpts whose original source is covered under another
    license and transformations of such derivatives. Note that mere excerpts
    by themselves (with the exception of raw source code) are not considered
    derivative works under this license. Use and redistribution is limited
    to the following conditions:
    .
    One may not create a derivative work which, in any way, violates the
    Paul Hsieh exposition license described above on the original content.
    .
    One may not apply a license to a derivative work that precludes
    anyone else from using and redistributing derivative content.
    .
    One may not attribute any derivative content to authors not involved
    in the creation of the content, though an attribution to the author
    is not necessary.
    License: HSIEH-BSD
    Copyright (c) 2010, Paul Hsieh
    All rights reserved.
    .
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    .
    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    .
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    .
    Neither my name, Paul Hsieh, nor the names of any other contributors
    to the code use may not be used to endorse or promote products
    derived from this software without specific prior written permission.
    .
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


## ISO Permission (ISO 8879) Identification Details

| Artefact                                | Usage / Reference                                                                                                        |
|-----------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| `lucene-analyzers-common-8.11.2.jar`    | Source Jar: `org/apache/lucene/analysis/charfilter/htmlentity.py`                                                        |
| `python-pygments-2.3.1+dfsg-1+deb10u2`  | Package copyright: `tests/dtds/*.ent`                                                                                    |
| `xwiki-commons-xml-15.10.4.jar`         | Binary Jar:<br>`[xwiki-commons-xml-15.10.4.jar]/xhtml-special.ent`<br>`[xwiki-commons-xml-15.10.4.jar]/xhtml-symbol.ent` |
| `xwiki-rendering-wikimodel-15.10.4.jar` | Source Jar: `org/xwiki/rendering/wikimodel/util/HtmlEntityUtil.java`                                                     |


## Package libsvtav1enc1-1.4.1+dfsg-1 Copyright File Extract

    Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
    Upstream-Name: SVT-AV1
    Source: https://gitlab.com/AOMediaCodec/SVT-AV1/
    License: BSD-3-Clause-Clear
    Files-Excluded: third_party/googletest
    Comment:
    In addition to the copyright license, SVT-AV1 contains the following patent
    license:
    .
    Alliance for Open Media Patent License 1.0
    .
    1. License Terms.
    .
    1.1. Patent License. Subject to the terms and conditions of this License, each
    Licensor, on behalf of itself and successors in interest and assigns,
    grants Licensee a non-sublicensable, perpetual, worldwide, non-exclusive,
    no-charge, royalty-free, irrevocable (except as expressly stated in this
    License) patent license to its Necessary Claims to make, use, sell, offer
    for sale, import or distribute any Implementation.
    .
    1.2. Conditions.


## Unicode Terms (2004) Identification Details

The license has been identified on the following artefacts:

* `lucene-analyzers-common-8.11.2.jar`
* `lucene-analyzers-kuromoji-8.11.2.jar`
* `lucene-analyzers-nori-8.11.2.jar`
* `lucene-analyzers-phonetic-8.11.2.jar`
* `lucene-backward-codecs-8.11.2.jar`
* `lucene-classification-8.11.2.jar`
* `lucene-codecs-8.11.2.jar`
* `lucene-core-8.11.2.jar`
* `lucene-expressions-8.11.2.jar`
* `lucene-grouping-8.11.2.jar`
* `lucene-highlighter-8.11.2.jar`
* `lucene-join-8.11.2.jar`
* `lucene-memory-8.11.2.jar`
* `lucene-misc-8.11.2.jar`
* `lucene-queries-8.11.2.jar`
* `lucene-queryparser-8.11.2.jar`
* `lucene-sandbox-8.11.2.jar`
* `lucene-spatial-extras-8.11.2.jar`
* `lucene-spatial3d-8.11.2.jar`
* `lucene-suggest-8.11.2.jar`
