# openDesk Report zur Abweichung der Open CoDE Lizenzcompliance

Im Zuge der Erstellung der Software Bill of Materials (SBOM) für die Container Distribution von openDesk haben sich wenige Abweichungen zu der [Open CoDE Lizenzcompliance Vorgabe](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/Lizenzcompliance) hinsichtlich freigegebener Open Source Lizenzen ergeben.

Im Folgenden werden diese Abweichungen für die durch OpenCoDE distribuierten Komponenten/Container aufgelistet. Ziel ist es, Transparenz für den Nutzer zu schaffen, auf welche Nutzungsrechte besonders geachtet werden sollte.

## Verständnis der unten stehenden Tabelle der Abweichungen

Eine Abweichung der Open CoDE Lizenzcompliance hinsichtlich der freigegebene Lizenzen bedeutet im Regelfall, dass ein Projekt oder Source Artefakt nicht oder nicht vollständig als "Open Source" im Sinne der [Open Source Definition](https://opensource.org/osd/) verstanden werden kann, es also Auflagen geben könnte, welche von den gängigen Open Source Auflagen abweichen.

Das Ziel von openDesk ist die sichere Nachnutzbarkeit von Software zu ermöglichen. Dabei wir die vollständige Open Source Lizenzierung des Codes und der Artefakte angestrebt.

Dennoch entstehen Abweichungen, zum Beispiel wenn eine Open Source ähnliche Lizenz im jeweiligen Ökosystem weit verbreitet ist, oder wenn eine Lizenz einzelne Rechte des Nutzers nicht explizit nennt, was oft bei älteren Lizenztexten der Fall ist. openDesk duldet in diesen Einzelfällen Verstöße gegen die eigenen Lizenzzielvorgaben, soweit dadurch keine praktischen Risiken für die Nutzer ersichtlich und ein Entfernen der Komponente nicht angemessen ist.

Für den Einsatz dieser Lizenzen als Teil von openDesk werden keine Konsequenzen für die Nutzer erwartet. Dennoch wird an dieser Stelle explizit auf diese Lizenzen hingewiesen, sodass die Abweichungen im Einzelfall geprüft werden können.

## Abweichungen

Die genaue Verortung der jeweiligen Lizenz ist den SBOM-Dateien zu entnehmen.

Lizenzname|Freigabestatus| Zusätzlicher Kommentar
---|---|---
Paul Hsieh Derivative | nicht freigegeben |
BSD-3-Clause-clear AND LicenseRef-scancode-alliance-open-media-patent-1.0 | nicht freigegeben |
Tiger Cryptography License  | nicht freigegeben | Nicht als Standalone Lizenz nutzbar. Im openDesk Kontext konsumiert, eingesetzt und distribuiert mit MIT
ISO 8879 | nicht freigegeben |
UnRar License | nicht freigegeben | Die UnRar Lizenz wird mit folgender Auflage in openDesk eingesetzt: UnRar must not be used to "develop RAR (WinRAR) compatible archiver and to re-create RAR compression algorithm" <br> Beides ist keine Intention von openDesk oder XWiki SAS und nicht naheliegend für die Nutzer des openDesk.
Unicode Terms (2004) | nicht freigegeben |
Bigelow and Holmes Luxi Fonts License | nicht freigegeben | openDesk setzt die Inhalte AS IS, unmodifiziert und ohne Add-Ons ein.
WS-Addressing Specification License | nicht freigegeben |
