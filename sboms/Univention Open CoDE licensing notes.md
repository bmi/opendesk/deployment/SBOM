# Notes on the licensing of documentation and software published by Univention on Open CoDE

1. The term "software" used in this document covers source code as well as binaries.

2. "Binaries" are versions of a computer program prepared for execution on a computer, possibly compiled (as opposed to source code). This includes compiled programs and libraries, packages in binary format of Linux distributions, installation packages, image files of virtual machines or container images for execution in environments such as OpenStack, VMware, Docker or Kubernetes.

3. These notes prevail any license information provided in documentation or source code headers with copyright by Univention.

4. Documentation and software published by Univention on Open CoDE are provided under the license conditions of the GNU Affero General Public License Version 3 (AGPL-3.0, https://www.gnu.org/licenses/agpl-3.0.de.html) with the exception of the Univention trademarks (see point 6) and with the exception of third-party open source software (see point 5).<br/>These license conditions of the AGPL-3.0 permit, among other things, the unrestricted use and distribution of the relevant binaries under certain conditions.

5. Publishing software on OpenCoDE does not imply the right to receive support, bug fixes, professional services or similar services. Univention points out that corresponding services can be acquired through commercial subscription contracts. See: https://www.univention.de/produkte/preise/<br/>
Univention would also like to point out that the subscription contracts offered by Univention do usually not allow a combination of Univention software under subscription and software not under subscription.<br/>
Without a subscription contract, warranty and liability claims against Univention are limited to the statutory rights based on the right of donation (§§ 516 pp BGB).

6. Software (binary and/or source code packages), such as image files of virtual machines or containers, may contain, in addition to the software developed by Univention, other open source programs and program libraries by third parties which are subject to other OSI-compliant licenses. The rights granted by the corresponding licenses of third parties continue to apply in full and are expressly not restricted by Univention.

7. “Univention", "be open." "UCS" and "Univention Corporate Server" and the corresponding logos are protected trademarks of Univention. Univention permits the use of these trademarks with regard to software published on Open CoDE only as long as the software has not been modified and only to the extent necessary for the intended use of the binaries, e.g. for the display of logos when running the software, as well as to distribute unmodified binaries. In particular, the trademarks may not be used for advertising purposes. Furthermore, the impression may not be created that the binaries are software officially supported by Univention through support, maintenance or other services (unless a corresponding subscription agreement exists).
